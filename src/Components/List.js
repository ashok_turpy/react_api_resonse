import React, { useState, useEffect } from 'react';
import axios from 'axios';


function ItemsList() {

    const [item, setItem] = useState({});

    useEffect(() => {
        const key = 'ISBN:9780980200447';
        const url = `https://openlibrary.org/api/books?bibkeys=${key}&jscmd=data&format=json`;
        axios.get(url).then((response) => {
            console.log("Response is =>", response);
            if(response?.status === 200){
                const res = response.data[key] || null;
                setItem(res);
            } else if(response?.status === 500 || response?.status === 503){
                alert("Internal Server Error");
            } else if(response?.status === 404){
                alert("Not Found");
            } else {
                alert("Something went wrong! Try Again!")
            }
        }).catch(error => {
            console.log(error?.response?.data?.error || error);
        });
    }, []);

    // console.log(item);

    return (
        <div>
            <center>
                <p>The Book Title is - <b>{item?.title}</b></p>
                <p>The Book Contains - <b>{item?.number_of_pages}</b> Pages</p>
                <img src={item?.cover?.medium} loading="lazy" alt="book" /><br />
            </center>
        </div>
    )
}

export default ItemsList;